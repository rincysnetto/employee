package com.jspiders.Employee.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.Employee.constants.AppConstants;
@Entity
@Table(name= AppConstants.Employee_Data)
public class EmployeeData implements Serializable {
	
	
	
	@Id
	@GenericGenerator(name="m_auto",strategy="increment")
	@GeneratedValue(generator="m_auto")
	@Column(name="Id")
	private Long id;
	
	@Column(name="Name")
	private String name;
	
	@Column(name="Age")
	private Integer age;
	
	@Column(name="Contact_number")
	private String contactNumber;
	
	@Column(name="Address")
	private String address;
	
	

	public EmployeeData() {
	
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "EmployeeData [id=" + id + ", name=" + name + ", age=" + age + ", contactNumber=" + contactNumber
				+ ", address=" + address + "]";
	}
	
	
	
	
	
	

}
