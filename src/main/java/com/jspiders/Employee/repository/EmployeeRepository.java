package com.jspiders.Employee.repository;

import java.util.List;

import org.hibernate.HibernateException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.jspiders.Employee.entity.EmployeeData;



public class EmployeeRepository {
	
	public static void saveDetails(EmployeeData e1)
	{
		try {
			Configuration cfg = new Configuration();
			 cfg.configure();
			 SessionFactory sessionFactory = cfg.buildSessionFactory();
			 Session session = sessionFactory.openSession();
			 Transaction transaction = session.beginTransaction();
			 session.save(e1);
			 transaction.commit();
			
			
			
		}catch (HibernateException e) {
		
			
		}	

}
	
	
	public static EmployeeData getEmployeeByID(Long id)
	{
		 
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		//Transaction transaction = session.beginTransaction();
		//EmployeeData e=session.get(EmployeeData.class,id);
		return session.get(EmployeeData.class,id); 
		//transaction.commit();no need of transaction,it only needs when updation happens
		
		//return e;
		
		
	}
	
	
	public EmployeeData updateRatingAndBudgetById(String contactNumber, String address, Long id)
	{
	 
		
			
			EmployeeData eData = getEmployeeByID(id);
			if (eData!=null) {
				eData.setAddress(address);
				eData.setContactNumber(contactNumber);
			}
			else System.out.println("update failed");
			
			Configuration configuration = new Configuration();
			
			
			configuration.configure();
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			session.saveOrUpdate(eData);
			session.merge(eData);
			
			
		
		return eData;
		
		
		
	}
	
	//By using hql....--->
	
	
	 public List<EmployeeData> findAll()
     {
		 Configuration configuration = new Configuration();
			configuration.configure();
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			String hql="from EmployeeData";
					Query query=session.createQuery(hql);
					return query.list();
     }
	 
	 public EmployeeData findByName(String empname)
	 {
		 Configuration configuration = new Configuration();
			configuration.configure();
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			String hql="from EmployeeData where name=:eName";
			Query query = session.createQuery(hql);
			query.setParameter("eName", empname);
			return  (EmployeeData) query.uniqueResult();
		 
	 }
	 
	 public void updateContactNumberAndAddressByName(String name,String contactNumber, String address)
	 {
		 Configuration configuration = new Configuration();
			configuration.configure();
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			String hql= "update EmployeeData set contactNumber=:c, address=:a where name=:m";
			Query query = session.createQuery(hql);
			query.setParameter("c", contactNumber);
			query.setParameter("a", address);
			query.setParameter("m", name );
			int rowsaffected = query.executeUpdate();
			transaction.commit();
			if(rowsaffected >0)
				System.out.println("updation successfull");
			else System.out.println("update failed");
	 }
				
			
		public void deleteByEmployeeName(String name)	{
			 Configuration configuration = new Configuration();
				configuration.configure();
				SessionFactory sessionFactory = configuration.buildSessionFactory();
				Session session = sessionFactory.openSession();
				Transaction transaction = session.beginTransaction();
				String hql="delete from EmployeeData where name=:n";
				Query query = session.createQuery(hql);
				query.setParameter("n", name);
				int rowsaffected = query.executeUpdate();
				transaction.commit();
				if(rowsaffected >0)
					System.out.println("updation successfull");
				else System.out.println("update failed");
		}
			
			
			
	 }

